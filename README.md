Smile On Air
=======
Il progetto “Smile On Air” nasce dalla collaborazione con l’istituto scientifico per la ricerca e la riabilitazione nell’età evolutiva “Eugenio Medea” – sezione di ricerca dell’associazione “La Nostra Famiglia” di Bosisio Parini (Lc). Il progetto si propone di realizzare un’applicazione in realtà aumentata (AR) per smartphone che permetta di animare oggetti tridimensionali interattivi quando lo smartphone inquadra delle immagini che sono poste in alcuni ambienti dell’Istituto E. Medea destinati alla riabilitazione dei bambini.
L’idea alla base del progetto è quella di realizzare un’applicazione che possa rendere più giocosa la permanenza dei bambini durante le terapie riabilitative, in modo da creare un ambiente più rilassante e divertente.
Questo progetto è stato affidato dagli studenti delle classi 3IA-B e 4IB, dell’indirizzo Informatica e Telecomunicazioni (articolazione Informatica) dell’IISS A. Greppi di Monticello Brianza nell’ambito del piano di alternanza scuola lavoro previsto dalla riforma della “Buona Scuola”, con la supervisione dei docenti di Informatica Proff. Gennaro Malafronte, Samuele Redaelli e Luca Melcarne.

Applicazione Android
--------------------
![](website/static/Screenshot_about.jpg)

Download
--------
## Download dell'[apk per Android][2]
## Download del [file in formato .pdf] [3] con le immagini target

Per installare l'applicazione seguire la procedura per l'installazione da sorgenti sconosciute, come riportato ad esempio in [questa guida][4]

#### Le immagini target andrebbero stampate su fogli e poi inquadrate con la fotocamera attraverso l'app; in alternativa le immagini target possono anche essere visualizzate sullo schermo di un altro smartphone,tablet, PC, video, etc.
## Video
[![Video dello spot di Smile On Air](website/static/SmileOnAirSpot.png)](https://youtu.be/isD-Lb00A-Q "Smile On Air Spot")
<br/>Smile on Air - the spot

[![Video dello spot di Smile On Air](website/static/SmileOnAirBackStage.png)](https://youtu.be/YEkwoVnDDn4 "Smile On Air BackStage")
<br/>Smile on Air - the backstage

## Screenshots
![](website/static/SplashScreen.jpg)
![](website/static/Screenshot_intro.jpg)
![](website/static/Screenshot_esempio_gioco.jpg)
![](website/static/Screenshot_il_mondo_animato.jpg)

Scarica l'app per Android e le immagini target per lanciare le animazioni e i giochi a realtà aumentata.
Scheda descrittiva del progetto
-------------------------------
La scheda descrittiva del progetto è scaricabile a questo [link][5]

Per ulteriori informazioni sul progetto contattare l'[Istituto Alessandro Greppi][1] di Monticello Brianza (Lc)




Licenza
--------
    Copyright 2018 IISS "Alessandro Greppi" - Monticello Brianza (Lc),
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
       http://www.apache.org/licenses/LICENSE-2.0
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 [1]: http://www.issgreppi.gov.it
 [2]: https://gitlab.com/GreppiShare/SmileOnAirBuild/raw/master/Build/Android/SmileOnAir.apk
 [3]: https://gitlab.com/GreppiShare/SmileOnAirBuild/raw/master/Targets/SmileOnAir_Targets.pdf
 [4]: https://www.chimerarevo.com/android/origini-sconosciute-android-247188/
 [5]: https://gitlab.com/GreppiShare/SmileOnAirBuild/raw/master/website/static/Progetto_SmileOnAir.pdf
